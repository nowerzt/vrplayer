#version 120
uniform mat4 bias;
uniform mat4 proj;
uniform mat4 modelview;
uniform mat3 normal_matrix;
uniform mat4 tex_matrix;

uniform mat4 lproj;
uniform mat4 lmodelview;

uniform vec3 light_pos;

varying vec4 lv;
varying vec3 l;
varying vec3 e;
varying vec3 v;
varying vec3 n;

uniform bool has_tangent;
attribute vec3 tangent;

uniform bool has_bones;
uniform mat4 bones[32];
varying vec4 bone_color;
attribute vec4 bone_id;
attribute vec4 bone_weight;

vec3 calc_tangent(vec3 n)
{
    vec3 v1 = cross(n, vec3(0,0,-1));
    vec3 v2 = cross(n, vec3(0,-1,0));
    return length(v1) > length(v2) ? v1 : v2;
}

void main()
{
    mat4 bonemat = mat4(1);
    ivec4 ids = ivec4(bone_id);
    vec4 vertex = gl_Vertex;
    vec4 normal = vec4(gl_Normal,0);
    if(has_bones)
    {
        bonemat = bones[ids[0]] * bone_weight[0] +
            bones[ids[1]] * bone_weight[1] +
            bones[ids[2]] * bone_weight[2] +
            bones[ids[3]] * bone_weight[3];
        vertex = bonemat * gl_Vertex;
        normal = bonemat * normal;
    }

    gl_Position = proj * modelview * vertex;
    gl_TexCoord[0] = tex_matrix * gl_MultiTexCoord0;
    
    n = normalize(normal_matrix * normal.xyz);
    v = vec3(modelview * vertex);
    
    vec3 t;
    if(has_tangent)
        t = normalize(normal_matrix * tangent);
    else
        t = normalize(normal_matrix * calc_tangent(normal.xyz));
    
    vec3 b = cross(n,t);
    mat3 mat = mat3(t.x,b.x,n.x, t.y,b.y,n.y, t.z,b.z,n.z);
    
    l = mat * normalize(light_pos - v);
    e = mat * normalize(-v);

    lv = bias * lproj * lmodelview * vertex;
    
    vec4 colors[] = vec4[] (
        vec4(1,0,0,1), vec4(0,1,0,1),
        vec4(0,0,1,1), vec4(1,1,0,1),
        vec4(0,1,1,1), vec4(1,1,1,1),
        vec4(0,.5,1,1), vec4(1,.5,1,1),
        vec4(0,1,.5,1), vec4(.5,.5,1,1),
        vec4(1,0,1,1), vec4(1,1,1,1)
    );
    bone_color = vec4(0);
    if(has_bones)
        for(int i = 0; i < 4; i++)
            bone_color += colors[int(bone_id[i])] * bone_weight[i];
}
