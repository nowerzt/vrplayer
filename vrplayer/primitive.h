#ifndef _OMI_PRIMITIVE_
#define _OMI_PRIMITIVE_

#define _USE_MATH_DEFINES

#ifdef WIN32
    #include <Windows.h>
    #include <GL\glew.h>
    #include <gl/GL.h>
#else
    #include <OpenGL/gl.h>
#endif

#include <math.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <string>
#include <vector>
#include <map>
#include <stdio.h>
#include <memory.h>
#include <cmath>

using namespace std;
using namespace glm;

struct omiVertex
{
    vec3 pos;
    vec2 tex;
    vec3 nor;
};

struct omiBone
{
    mat4 mat, offset;
    mat4 transform;
    mat4 computedMat;
    omiBone *parent;
    string name;
    
    mat4 trans();
    mat4 ptrans();
};

struct omiKeyframe
{
    float time;
    quat rotation;
};

struct omiMaterial
{
    GLuint diffuse_tex;
    GLuint normal_tex;
    GLuint specular_tex;
    GLuint shadow_tex;
    
    omiMaterial();
};

struct omiElement
{
    GLuint ibuf,vbuf;
    omiMaterial material;
    int i_count;
    int v_stride;
    long p_offset;
    long t_offset;
    long n_offset;
    long tg_offset;
    long bw_offset;
    long bid_offset;
    mat4 model;
    mat4 texmat;
    bool shaded;
    bool has_tangent;
    bool has_bones;
    bool has_animation;
    int animFrame;
    int animFramesCount;
    vec4 color;
    map<string, omiBone> bones;
    vector<omiBone*> bones_vector;
    map<string, vector<omiKeyframe>> keyframes;

    omiElement();
    
    virtual void draw(GLenum type) const;
    virtual void drawBones(GLenum type);
    virtual void stepAnim();
};

struct omiPlane : public omiElement
{
    void create(GLfloat w, GLfloat h);
};

struct omiMesh : public omiElement
{
    string name;
};

struct omiSphere : public omiElement
{
    void create(float radius, int divx, int divy);
    virtual void draw(GLenum type);
};

struct omiGroup
{
    vector<omiMesh> meshes;
    mat4 model;
    GLuint shadowTexture;
    void setShadowTexture(GLuint tid);
};

#endif
