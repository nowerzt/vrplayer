#version 120
uniform mat4 proj;
uniform mat4 view;
varying vec4 texcoord;void main()
{
    gl_Position = proj * view * gl_ModelViewMatrix * gl_Vertex;
    texcoord = gl_MultiTexCoord0;
}
