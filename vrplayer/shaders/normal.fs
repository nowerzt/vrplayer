#version 120
uniform sampler2D diffuse_tex;
uniform sampler2D normal_tex;
uniform sampler2D specular_tex;
uniform sampler2DShadow shadow_tex;
uniform vec4 light_pos;

varying vec3 l;
varying vec3 e;

void main()
{
	vec4 color = texture2D(diffuse_tex, gl_TexCoord[0].st);
	vec4 norm = texture2D(normal_tex, gl_TexCoord[0].st)*2-1;
	vec4 spec = texture2D(specular_tex, gl_TexCoord[0].st);
    
	vec3 L = normalize(l);
	vec3 E = normalize(e);
	vec3 N = vec3(0,0,1);//normalize(norm.rgb);
	vec3 R = normalize(reflect(-L, N));
    
	float Ispec = clamp(pow(max(dot(R,E),0),10),0,1);
	float Idiff = clamp(max(dot(N,L),0),0,1);
    
    gl_FragColor = color * (Idiff + Ispec);
}

