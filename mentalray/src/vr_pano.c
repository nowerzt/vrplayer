// VR Pano for Mentalray
// by Omar Mohamed Ali Mudhir 
// omarator@gmail.com / @omigamedev

#include <math.h>
#include <shader.h>
#include "vr_pano.h"

DLLEXPORT int vr_pano_version()
{
	return 1;
}

DLLEXPORT void vr_pano_init(miState* state, vr_pano_params* in_pParams, miBoolean* inst_req)
{
	if(!in_pParams) 
	{		
		*inst_req = miFALSE;		
	} 
}

DLLEXPORT miBoolean vr_pano(miColor* out_pResult, miState* state, vr_pano_params* in_pParams)
{
	miScalar uval, vval, rayx, rayy, rayz;
	miVector raydir, raydir_internal; 
    miVector rayorig, rayorig_internal;
    miScalar sgn = 1.0f;
	miScalar eye_dist = 0.2f;
    
	eye_dist = *mi_eval_scalar(&in_pParams->m_dist);

    uval = state->raster_x / state->camera->x_resolution;
	vval = 2.0f * (state->camera->y_resolution - state->raster_y) / state->camera->y_resolution;
    
    if (vval > 1.f)
    {
        vval -= 1.f;
        sgn = -1.f;
    }

    miScalar anglex = (miScalar)(M_PI * (2.0 * uval + 0.5));
    miScalar angley = (miScalar)(M_PI * vval);

    miScalar sx = sinf(anglex);
    miScalar cx = cosf(anglex);

    rayx = sinf(angley) * cx;
    rayy = cosf(angley);
    rayz = sinf(angley) * sx;

	raydir.x = rayx; raydir.y = rayy; raydir.z = rayz;

	mi_vector_from_camera(state, &raydir_internal, &raydir);
    
    rayorig.x = sgn * sx * eye_dist;
    rayorig.y = 0;
    rayorig.z = -sgn * cx * eye_dist;
    mi_point_from_camera(state, &rayorig_internal, &rayorig);

    return mi_trace_eye(out_pResult, state, &rayorig_internal, &raydir_internal);
}

DLLEXPORT void vr_pano_exit(miState* state, vr_pano_params* in_pParams)
{
}
