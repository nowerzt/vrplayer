#version 120
uniform mat4 proj;
uniform mat4 modelview;
varying vec4 texcoord;

void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    texcoord = gl_MultiTexCoord0;
}
