#include "primitive.h"
#include "shader.h"
#include <math.h>

mat4 omiBone::trans()
{
    omiBone *p = parent;
    mat4 ret = computedMat;
    while(p)
    {
        ret = p->computedMat * ret;
        p = p->parent;
    }
    return ret;
}

mat4 omiBone::ptrans()
{
    return parent ? parent->trans() : mat4();
}

omiElement::omiElement()
{
    ibuf = 0;
    vbuf = 0;
    i_count = 0;
    p_offset = 0;
    t_offset = 0;
    n_offset = 0;
    v_stride = 0;
    shaded = true;
    has_tangent = false;
    has_bones = false;
    has_animation = false;
    animFrame = 0;
    animFramesCount = 0;
    color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
}

void omiElement::draw(GLenum type) const
{
    glActiveTexture(TEX_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, material.diffuse_tex);
    
    glActiveTexture(TEX_NORMAL);
    glBindTexture(GL_TEXTURE_2D, material.normal_tex);
    
    glActiveTexture(TEX_SPECULAR);
    glBindTexture(GL_TEXTURE_2D, material.specular_tex);
    
    glActiveTexture(TEX_SHADOW);
    glBindTexture(GL_TEXTURE_2D, material.shadow_tex);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbuf);
    glVertexPointer(3, GL_FLOAT, v_stride, (void*)p_offset);
    glNormalPointer(GL_FLOAT, v_stride, (void*)n_offset);
    glTexCoordPointer(2, GL_FLOAT, v_stride, (void*)t_offset);
    
    if(has_tangent)
    {
        glEnableVertexAttribArray(ATT_TANGENTS);
        glVertexAttribPointer(ATT_TANGENTS, 3, GL_FLOAT, GL_FALSE, v_stride, (void*)tg_offset);
    }
    
    if(has_bones)
    {
        glEnableVertexAttribArray(ATT_BONE_ID);
        glEnableVertexAttribArray(ATT_BONE_WEIGHT);
        glVertexAttribPointer(ATT_BONE_ID, 4, GL_FLOAT, GL_FALSE, v_stride, (void*)bid_offset);
        glVertexAttribPointer(ATT_BONE_WEIGHT, 4, GL_FLOAT, GL_FALSE, v_stride, (void*)bw_offset);
    }
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf);
    glDrawElements(type, i_count, GL_UNSIGNED_INT, 0);
    
    glDisableVertexAttribArray(ATT_TANGENTS);
    glDisableVertexAttribArray(ATT_BONE_ID);
    glDisableVertexAttribArray(ATT_BONE_WEIGHT);
}

static void print4(const char *s, mat4 m)
{
    printf("%s\n", s);
    for(int i = 0; i < 4; i++)
        printf("%.3f %.3f %.3f %.3f\n", m[0][i], m[1][i], m[2][i], m[3][i]);
}

void omiElement::drawBones(GLenum type)
{
    glLineWidth(5);
    glPointSize(10);
    glBegin(type);
    map<string,omiBone>::iterator iter;
    for(iter = bones.begin(); iter != bones.end(); ++iter)
    {
        omiBone &b = iter->second;
        if(b.parent)
        {
            vec4 tr0 = b.trans() * vec4(0,0,0,1);
            vec4 tr1 = b.ptrans() * vec4(0,0,0,1);
            glVertex3f(tr0.x, tr0.y, tr0.z);
            glVertex3f(tr1.x, tr1.y, tr1.z);
        }
    }
    glEnd();
}

void omiElement::stepAnim()
{
    if(animFramesCount)
        animFrame = (animFrame + 1) % animFramesCount;
    
    map<string,omiBone>::iterator iter;
    for(iter = bones.begin(); iter != bones.end(); ++iter)
    {
        omiBone &b = iter->second;
        if(keyframes.count(b.name))
        {
            if(animFrame < keyframes[b.name].size())
            {
                b.computedMat = mat4_cast(keyframes[b.name][animFrame].rotation);
                b.computedMat[3][0] = b.mat[3][0];
                b.computedMat[3][1] = b.mat[3][1];
                b.computedMat[3][2] = b.mat[3][2];
            }
        }
        else
            b.computedMat = b.mat;
    }
}

void omiPlane::create(GLfloat w, GLfloat h)
{
    vec3 pos[4], nor[4], tan[4];
    vec2 tex[4];
    GLuint idx[6];
    w *= 0.5f;
    h *= 0.5f;
    GLfloat z = 0.0f;
    pos[0] = vec3(-w,-h,z);
    pos[1] = vec3( w,-h,z);
    pos[2] = vec3( w, h,z);
    pos[3] = vec3(-w, h,z);
    tex[0] = vec2(0,0);
    tex[1] = vec2(1,0);
    tex[2] = vec2(1,1);
    tex[3] = vec2(0,1);
    nor[0] = vec3(0,0,1);
    nor[1] = vec3(0,0,1);
    nor[2] = vec3(0,0,1);
    nor[3] = vec3(0,0,1);
    tan[0] = vec3(0,0,1);
    tan[1] = vec3(0,0,1);
    tan[2] = vec3(0,0,1);
    tan[3] = vec3(0,0,1);

    GLuint indexes[] = { 0, 1, 2, 2, 3, 0 };
    memcpy(idx, indexes, sizeof(idx));
    
    long i_size = sizeof(idx);
    long p_size = sizeof(pos);
    long n_size = sizeof(nor);
    long tg_size = sizeof(tan);
    long t_size = sizeof(tex);

    glGenBuffers(1, &ibuf);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, &idx[0], GL_STATIC_DRAW);
    
    i_count = 6;
    p_offset = 0;
    n_offset = p_size;
    tg_offset = p_size + n_size;
    t_offset = p_size + n_size + tg_size;
    glGenBuffers(1, &vbuf);
    glBindBuffer(GL_ARRAY_BUFFER, vbuf);
    glBufferData(GL_ARRAY_BUFFER, p_size+n_size+t_size+tg_size, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, p_offset, p_size, &pos[0]);
    glBufferSubData(GL_ARRAY_BUFFER, n_offset, n_size, &nor[0]);
    glBufferSubData(GL_ARRAY_BUFFER, tg_offset, tg_size, &tan[0]);
    glBufferSubData(GL_ARRAY_BUFFER, t_offset, t_size, &tex[0]);
}

void omiGroup::setShadowTexture(GLuint tid)
{
    shadowTexture = tid;
    vector<omiMesh>::iterator iter;
    for(iter = meshes.begin(); iter != meshes.end(); ++iter)
    {
        iter->material.shadow_tex = shadowTexture;
    }
}

omiMaterial::omiMaterial()
{
    diffuse_tex = 0;
    normal_tex = 0;
    specular_tex = 0;
    shadow_tex = 0;
}

void omiSphere::create(float radius, int rings, int sectors)
{
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> texcoords;
    std::vector<GLuint> indices;

    float const R = 1./(float)(rings-1);
    float const S = 1./(float)(sectors-1);
    int r, s;
    
    vertices.resize(rings * sectors * 3);
    normals.resize(rings * sectors * 3);
    texcoords.resize(rings * sectors * 2);
    std::vector<GLfloat>::iterator v = vertices.begin();
    std::vector<GLfloat>::iterator n = normals.begin();
    std::vector<GLfloat>::iterator t = texcoords.begin();
    for(r = 0; r < rings; r++) for(s = 0; s < sectors; s++) {
        float const y = sin( -M_PI_2 + M_PI * r * R );
        float const x = cos(2*M_PI * s * S) * sin( M_PI * r * R );
        float const z = sin(2*M_PI * s * S) * sin( M_PI * r * R );
        
        *t++ = s*S;
        *t++ = r*R;
        
        *v++ = x * radius;
        *v++ = y * radius;
        *v++ = z * radius;
        
        *n++ = x;
        *n++ = y;
        *n++ = z;
    }
    
    indices.resize(rings * sectors * 4);
    std::vector<GLuint>::iterator i = indices.begin();
    for(r = 0; r < rings-1; r++) for(s = 0; s < sectors-1; s++) {
        *i++ = r * sectors + s;
        *i++ = r * sectors + (s+1);
        *i++ = (r+1) * sectors + (s+1);
        *i++ = (r+1) * sectors + s;
    }
    
    long i_size = indices.size() * sizeof(unsigned int);
    long p_size = vertices.size() * sizeof(float);
    long n_size = normals.size() * sizeof(float);
    long t_size = texcoords.size() * sizeof(float);
    
    glGenBuffers(1, &ibuf);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, indices.data(), GL_STATIC_DRAW);
    
    i_count = (int)indices.size();
    p_offset = 0;
    n_offset = p_offset + p_size;
    t_offset = n_offset + n_size;
    long tot_size = t_offset + t_size;
    glGenBuffers(1, &vbuf);
    glBindBuffer(GL_ARRAY_BUFFER, vbuf);
    glBufferData(GL_ARRAY_BUFFER, tot_size, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, p_offset, p_size, vertices.data());
    glBufferSubData(GL_ARRAY_BUFFER, n_offset, n_size, normals.data());
    glBufferSubData(GL_ARRAY_BUFFER, t_offset, t_size, texcoords.data());
    
    has_tangent = false;
    has_bones = false;
    has_animation = false;
}

void omiSphere::draw(GLenum type)
{
    omiElement::draw(GL_QUADS);
}
