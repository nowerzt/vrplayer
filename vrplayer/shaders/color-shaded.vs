uniform mat4 proj;
uniform mat4 modelview;
uniform mat3 normal_matrix;
uniform vec4 color;
uniform vec4 light_pos;
varying vec4 c;

void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    vec3 v = (modelview * gl_Vertex).xyz;
    vec3 N = normalize(normal_matrix * gl_Normal);
    vec3 L = normalize(vec3(light_pos - gl_Position));
    float diffuse = max(dot(N,L), 0.0);
    c = color * diffuse;
}
